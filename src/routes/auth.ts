import { Router, Request, Response, NextFunction } from 'express';
import User from '../services/User';
import Freelancer from '../services/Freelancer';
import Customer from '../services/Customer';

import catchError from '../services/helpers/catchError';

const Auth = Router();


Auth.get('/reg', catchError(async (req, res) => {
  let type = req.body.type;
  let userInfo = {
    name: req.body.name,
    password: req.body.password,
    email: req.body.email
  }
  let token: string;
  if(type == 'freelancer') token = await Freelancer.reg(userInfo);
  else if (type == 'customer') token = await Customer.reg(userInfo);
  else throw new Error('Invalid type')
  res.status(200).json({ status: 'OK', token })
}));

Auth.get('/login', catchError(async (req, res) => {
  let type = req.body.type;
  let password = req.body.password;
  let email = req.body.email;
  let name = req.body.name;
  let token: string;
  if(type == 'freelancer') token = await Freelancer.login(password, name, email);
  else if (type == 'customer') token = await Customer.login(password, name, email);
  else throw new Error('Invalid type')
  if (token === null) throw new Error('User did not found in the DB')
  res.status(200).json({ status: 'OK', token })
}))


Auth.get('/getUserInfo', catchError(async (req, res) => {
  let token = req.body.token;
  let user = await User.auth(token);
  res.status(200).json({ status: 'OK', data: user.user });
}))


export default Auth;