import * as express from 'express';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';

import AuthRouter from './routes/auth';

const App = express();
App.use(cors());
App.use(bodyParser.json());

App.use('/auth', AuthRouter)

export default App;