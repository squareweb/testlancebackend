import Validator from './helpers/validator';
import * as md5 from 'md5';

import freelancerSchema from '../models/Freelancer';
import customerSchema from '../models/Customer';
import * as jwt from 'jsonwebtoken';

import Customer from './Customer';
import Freelancer from './Freelancer';

export default class User {

  static typeOfUser = null;
  readonly userType = null;
  readonly user = null;

  protected constructor(userFromDb, type) {


    this.user = userFromDb;
    this.userType = type == 'freelancer' ? 'freelancer' : 'customer';


  }


  static async auth(token: string) {
    let userDecoded: any = await jwt.verify(token, process.env.SECRET_KEY);
    let userFromDb: any;
    
    
    try {
      if(userDecoded.type == 'freelancer') userFromDb = await freelancerSchema.findById(userDecoded._id);
      else if(userDecoded.type == 'customer') userFromDb = await customerSchema.findById(userDecoded._id);
    } catch(err) {
      throw new Error(err.message);
    }

    return this.callNewClass(userFromDb, userDecoded.type);
  }

  static callNewClass(userFromDb, type) {
    return new User(userFromDb, type);
  }

  static async reg(props: { name: string, password: string, email: string }, type: string = this.typeOfUser) {
    for(let prop in props) {
      switch(prop) {
        case 'name': 
          if(!Validator.checkUsername(props[prop])) throw new Error('Name is not correct');
          break;
        case 'password': 
          if(!Validator.checkPassword(props[prop])) throw new Error('Password is not correct');
          break;
        case 'email': 
          if(!Validator.checkEmail(props[prop])) throw new Error('Email is not correct');
          break;
      }
    }


    let newUser;

    if(type == 'freelancer') {
      newUser = new freelancerSchema({
        ...props,
        password: md5(props.password)
      })
    }
    else if(type == 'customer') {
      newUser = new customerSchema({
        ...props,
        password: md5(props.password)
      })
    }

    let result = await newUser.save();

    let token: string

    try {
      token = await jwt.sign({ type:  type == 'freelancer' ? 'freelancer' : 'customer', name: props.name, email: props.email, _id: result._id }, process.env.SECRET_KEY, { expiresIn: `${process.env.TOKEN_LIFETIME.toString()}d` });
    } catch(err) {
      throw new Error(err.message);
    }
    
    return token;
    
  }

  static async login(password, name?, email?, setType?) {
    let type = setType ? setType : this.typeOfUser;
    let fetchedUser: any;
    try {
      if(type == 'freelancer') fetchedUser = await freelancerSchema.findOne({ [name ? 'name' : 'email']: name ? name : email, password: md5(password) }); 
      else if(type == 'customer') fetchedUser = await customerSchema.findOne({ [name ? 'name' : 'email']: name ? name : email, password: md5(password) }); 
    } catch(err) {
      throw new Error(err);
    }
    if(fetchedUser === null) return null;
    let token: string = await jwt.sign({ type:  type == 'freelancer' ? 'freelancer' : 'customer', name: fetchedUser.name, email: fetchedUser.email, _id: fetchedUser._id }, process.env.SECRET_KEY, { expiresIn: `${process.env.TOKEN_LIFETIME.toString()}d` });
    return token;
  }

  async addMoney(sum: number) {
    this.user.balance = this.user.balance + sum;
    await this.user.save();
  }

  async change(propsToChange: object) {
    for(let prop in propsToChange) {
      this.user[prop] = propsToChange[prop];
    }
    await this.user.save()
  }
}