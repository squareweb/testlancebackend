

class Validator {
  static emailRegularExpression = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
  static passwordRegularExpression = /^.*((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(.*[!@#$%^&*()_+=])).*$/i;
  static usernameRegularExpression = /^[\w\s-]{3,30}$/;

  static checkEmail(email: string): boolean {
    if(this.emailRegularExpression.test(email)) return true;
    else return false;
  }

  static checkPassword(password: string): boolean {
    if(this.passwordRegularExpression.test(password)) return true;
    else return false;
  }

  static checkUsername(username: string): boolean {
    if(this.usernameRegularExpression.test(username)) return true;
    else return false;
  }
}


export default Validator;