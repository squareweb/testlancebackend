const catchError = (func: (req, res, next) => void) => {
 return async (req, res, next) => {
   try {
     await func(req, res, next);
   } catch(err) {
     return res.status(500).json({ status: 'ERROR', error: err.message})
   }
 }
}

export default catchError;