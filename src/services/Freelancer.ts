import User from './User';

export default class Freelancer extends User {
  static typeOfUser = 'freelancer';

  static callNewClass(userFromDb, type) {
    return new Freelancer(userFromDb, type);
  }
}