import User from './User';

export default class Customer extends User {
  static typeOfUser = 'customer';

  static callNewClass(userFromDb, type) {
    return new Customer(userFromDb, type);
  }
}