import * as mongoose from 'mongoose';


const FreelancerSchema = new mongoose.Schema({
  name: { type: String, required: true },
  password: { type: String, required: true },
  email: { type: String, required: true },
  balance: { type: Number, required: true, default: 0 }
});


const Customer = mongoose.model('Freelancer', FreelancerSchema);


export default Customer;