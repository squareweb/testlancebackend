import * as http from 'http';
import * as mongoose from 'mongoose';

import App from './app';

const MONGO_URL: string = process.env.MONGO_URL;
const PORT: number = Number.parseInt(process.env.PORT);

mongoose.connect(MONGO_URL, (err: any) => {
  if(err) console.warn(err.message)
  else console.log('Connected to MongoDB')
})

http.createServer(App).listen(PORT, () => console.log(`Server is listening on the port - ${process.env.PORT}`))